const express = require("express");
const router = express.Router();

const auth = require("../auth");
const userController = require("../controllers/userController");

const Course = require("../models/Course")

// Check the email
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});

// Register a user
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving user details
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route for enrolling a user
router.post("/enroll", auth.verify, (req,res) => {

	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;