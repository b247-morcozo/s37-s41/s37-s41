const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");

const auth = require("../auth");

// Route for create a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController))
});

//route for retrieving all the courses
router.get("/all", (req, res) => {

	courseController.getAllCourse().then(resultFromController => res.send(resultFromController));
})

//Route for retrieving all active course
router.get("/active", (req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific course
router.get("/:courseId/details", (req,res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

//Route for updating a course
router.put("/:id/update", auth.verify, (req,res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for archiving a course
router.patch("/:courseId/archive", auth.verify, (req,res) => {

	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;

